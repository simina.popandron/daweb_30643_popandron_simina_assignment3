import React from "react";
import simina from '../commons/images/simina.png';

class ProfilStudent extends React.Component {
    render(){

        let content = {
            English: {
                cine: "Who am I?",
                teh:"Technologies of interest"
            },
            Romana: {
                cine: "Cine sunt eu?",
                teh:"Tehnologii de interes"
            }
        };
        localStorage.getItem("language") === "Romana" ? (content = content.Romana) : (content = content.English);

        return(
            <div className="container-fluid page_background_color full text-center container portfolio" >
                    <div className="row">
                        <div className="col-md-12">
                            <div className="heading">
                                <img src="https://image.ibb.co/cbCMvA/logo.png"/>
                            </div>
                        </div>
                    </div>
                    <div className="bio-info">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="bio-image">
                                            <img src={simina} alt="simina" width="250" height="250"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="bio-content">
                                    <h3>{content.cine}</h3>
                                    <h5>Simina Diana Popandron</h5>
                                    <br/>
                                    <h6>{content.teh}</h6>

                                    <div className="list-type1">
                                        <ol>
                                            <li><a href="#">Android Studio & Android Developmer Tools</a></li>
                                            <li><a href="#">Java</a></li>
                                            <li><a href="#">Kotlin</a></li>
                                        </ol>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

            </div>
            );
    }
}

export default ProfilStudent;