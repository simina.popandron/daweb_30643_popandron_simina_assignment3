import React, { Component } from 'react';
import profile from './commons/images/profile.png';
import NavItem from "react-bootstrap/NavItem";
import { getIsLoggedInFromLocalStorage, setIsLoggedInLocalStorage} from './Helpers';
import {Redirect} from "react-router";
import {
    Navbar,
    NavLink,
} from 'reactstrap';
import {Button} from "react-bootstrap";

function storeLoginStateInLocalStorage(state) {
    localStorage.setItem("state", state);
}

export default class NavigationBar extends Component {
    state = {
        loginIsVisible: false,
        isLoggedIn: getIsLoggedInFromLocalStorage()};

        handleSelect = eventKey => {
        if (eventKey === 'logout') {
            setIsLoggedInLocalStorage('false');
            this.setState({isLoggedIn: 'false'});
            window.location=window.location.href;
        } else if (eventKey === 'login') {
            this.setState({loginIsVisible: true});
        }
    };

    render(){
        let content = {
            English: {
                n1:"Home",
                n2:"News",
                n3:"About",
                n4:"Profile",
                n5:"Coordinator",
                n6:"Contact",
                n7:"Language"
            },
            Romana: {
                n1:"Acasa",
                n2:"Noutati",
                n3:"Despre lucrare",
                n4:"Profil student",
                n5:"Coordonator",
                n6:"Contact",
                n7:"Limba"
            }
        };
        localStorage.getItem("language") === "Romana" ? (content = content.Romana) : (content = content.English);

        function handleLogout(event) {
            localStorage.clear();
            localStorage.removeItem('id');
            setIsLoggedInLocalStorage('false');
            document.location.reload();
            return <Redirect to="/" />
        }

        let loginNavLink, registerNavLink;
        const isLoggedIn = this.state.isLoggedIn;
        if (isLoggedIn === 'true') {
            loginNavLink =  <form onSubmit={handleLogout}>
                                <Button block bsSize="large"  type="submit"> Logout</Button>
                            </form>;
            registerNavLink = <NavLink eventKey='profile'  className="text_navbar"  href="/profilUser"  onSelect={e => this.handleSelect(e)}>User Profile</NavLink>;
            storeLoginStateInLocalStorage(this.state.isLoggedIn);

        } else {
            loginNavLink = <NavLink eventKey='login' className="text_navbar"  href="/login" onSelect={e => this.handleSelect(e)}>Login</NavLink>;
            registerNavLink = <NavLink eventKey='register' className="text_navbar"  href="/register" onSelect={e => this.handleSelect(e)}>Register</NavLink>;
        }
        console.log(this.state.isLoggedIn);
        return(
        <div>
            <Navbar className="color_navbar navbar-inverse navbar-fixed-top nav-tabs " light expand="md">

                    <NavItem >
                        <NavLink href="/">
                            <img src={profile} className="logo"/>
                        </NavLink>
                    </NavItem>
                    <NavItem >
                        <NavLink href="/" className="text_navbar" >{content.n1} </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/noutati" className="text_navbar">{content.n2} </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/despreLucrare" className="text_navbar">{content.n3}</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/profilStudent" className="text_navbar">{content.n4}</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/coordonator" className="text_navbar">{content.n5}</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/contact" className="text_navbar">{content.n6}</NavLink>
                    </NavItem>
                    <NavItem className="text_navbar">
                        {registerNavLink}
                    </NavItem>
                    <NavItem className="text_navbar">
                        {loginNavLink}
                    </NavItem>
            </Navbar>
        </div>
        );
}}


